class people::gregpardo {
  include sublime_text
  include iterm2::dev
  include zsh
  include tower
  include imagemagick
  include graphicsmagick
  include go
  include fortune
  include macvim
  include ngrok
  include dropbox
  
  /*
  $user = $::boxen_user
  $userhome = "/Users/${::luser}"

  file { "${userhome}/.homesick" :
    ensure        => directory,
    owner         => $username,
    recurse       => true
  }

  repository { 'homeshick':
    source => 'andsens/homeshick',
    path   => "${userhome}/.homesick/repos/homeshick"
  }
  -> file { "${HOME}/.homeshick":
    ensure => 'link',
    target => "${userhome}/.homesick/repos/homeshick/home/.homeshick"
  }
  -> repository { 'dotfiles':
    source => 'https://github.com/gregpardo/dotfiles.git',
    path   => "${userhome}/.homesick/repos/dotfiles"
  }
  ~> exec { "${userhome}/.homeshick link dotfiles --force":
    refreshonly => true
  }

   ## Run homeshick
  exec { "Run homeshick refresh":
    provider => 'shell',
    command => "${userhome}/.homesick/repos/homeshick/bin/homeshick --quiet refresh",
    cwd => "${userhome}"
  }*/
}
